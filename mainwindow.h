#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

/// Foward declaration. Esto hace más rápida la compilación. Para que funcione correctamente solo se pueden declarar apuntadores o referencias de esas clases, ya que el compilador no sabe que tamaño tiene.
class QLabel;
class QString;
class QStringList;
class QListWidgetItem;

class MainWindow : public QMainWindow {
		Q_OBJECT // Qt siempre necesita esta macro, le da información que necesita el MOC (no estoy seguro)

	public:
		//Ctor
		explicit MainWindow (QWidget* parent = 0);
		//Dtor
		~MainWindow();

	protected:
		/// Hace las conexiones del programa
		void setupActions();
		/// Función utilitaria que pregunta por cambios en el programa cuando se quiere abrir un nuevo archivo
		bool mayDiscardDocument();
		/// Función que salva el contenido del archivo en disco
		void saveFile (const QString&);
		/// Función que rellena la lista del dock de templates
		void fillList();
		/// Función que lee las configuraciones del usuario
		void readSettings();
		/// Función que escribe las configuraciones del usuario
		void writeSettings();

	protected slots:
		void newFile();
		void loadFile();
		void saveFile();
		void saveFileAs();
		void undo();
		void redo();
		void copy();
		void about();
		void updateStats();
		void insertText (QListWidgetItem* item);

	private:
		Ui::MainWindow* ui;
		QString rutaArchivo;
		QLabel* statsLabel; // Contendrá el número de caracteres y palabras del documento
};

#endif // MAINWINDOW_H
