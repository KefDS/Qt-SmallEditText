#include <QFileDialog>
#include <QtGui>
#include <QLabel>
#include <QSettings>
#include <QStringList>
#include <QTextCursor>
#include <QMessageBox>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow (QWidget* parent) :
	QMainWindow (parent),
	ui (new Ui::MainWindow),
	statsLabel (new QLabel (this)) {
	ui->setupUi (this); // Método que le aplica el form a esta clase

	setupActions();

	/// Existen 3 tipo de mensajes en el statusBar: temporales, normales & permanentes
	/// Los 2 primeros se alojan en la parte izquierda del toolbar. Los permanentes se ubican en el lado derecho
	statusBar()->addPermanentWidget (statsLabel);

	fillList(); // Llena la lista de los textos que tendrá el dock
	readSettings(); // Carga las configuraciones
}

MainWindow::~MainWindow() {
	writeSettings();
	delete ui;
}

/// Método que asigna los signals-slots
void MainWindow::setupActions() {

	/*We link the quit action with the quit() signal of the QApplication object, which is
	 * accessible from the entire application via the global pointer qApp*/
	connect (ui->action_Quit, SIGNAL (triggered (bool)),
			 qApp, SLOT (quit()));

	// Conecta los signals (botón presionado) con los slots correspondientes
	connect (ui->action_Open, SIGNAL (triggered (bool)),
			 this, SLOT (loadFile()));
	connect (ui->actionSave, SIGNAL (triggered (bool)),
			 this, SLOT (saveFile()));
	connect (ui->actionSave_As, SIGNAL (triggered (bool)),
			 this, SLOT (saveFileAs()));
	connect (ui->action_New, SIGNAL (triggered (bool)),
			 this, SLOT (newFile()));

	// Qt utiliza por defecto el undo, redo, copy, etc
	connect (ui->textEdit, SIGNAL (copyAvailable (bool)),
			 ui->action_Copy, SLOT (setEnabled (bool)));
	connect (ui->textEdit, SIGNAL (undoAvailable (bool)),
			 ui->action_Undo, SLOT (setEnabled (bool)));
	connect (ui->textEdit, SIGNAL (redoAvailable (bool)),
			 ui->action_Repeat, SLOT (setEnabled (bool)));

	// Operacion undo, redo, copy
	connect (ui->action_Copy, SIGNAL (triggered (bool)),
			 this, SLOT (copy()));
	connect (ui->action_Undo, SIGNAL (triggered (bool)),
			 this, SLOT (undo()));
	connect (ui->action_Repeat, SIGNAL (triggered (bool)),
			 this, SLOT (redo()));

	// Acerca de...
	connect (ui->actionInfo, SIGNAL (triggered (bool)),
			 this, SLOT (about()));

	// Permanent widget
	connect (ui->textEdit, SIGNAL (textChanged()), this, SLOT (updateStats()));

	// Dock
	connect (ui->templateList, SIGNAL (itemActivated (QListWidgetItem*)), this, SLOT (insertText (QListWidgetItem*)));
}

void MainWindow::newFile() {
	if (!mayDiscardDocument()) {
		return;
	}
	ui->textEdit->setPlainText ("");
	rutaArchivo = "";
}

void MainWindow::loadFile() {
	/// Se abre el archivo a editar. La función estática QFileDialog::getOpenFileName muestra un menú para seleccionar un archivo
	QString filename = QFileDialog::getOpenFileName (this);
	QFile file (filename);

	// QIODevice::Text bandera para leer texto en diferentes S.O
	if (file.open (QIODevice::ReadOnly | QIODevice::Text)) {
		ui->textEdit->setPlainText (QString::fromUtf8 (file.readAll()));	// Se asume que está en UTF-8
		rutaArchivo = filename;
		statusBar()->showMessage (tr ("Archivo cargado correctamente"), 3000); // Mensaje temporal
	}
}

bool MainWindow::mayDiscardDocument() {
	if (ui->textEdit->document()->isModified()) {
		QString ruta = rutaArchivo;
		if (ruta.isEmpty()) {
			ruta = tr ("Unnamed");
		}

		// Cuadro de texto con sí o no
		if (QMessageBox::question (this, tr ("Guardar Documento"),
								   tr ("No ha guardado su documento actual, si crea un nuevo documento "
									   "puede perder los cambios de el documento actual"),
								   tr ("Guardar documento"), tr ("Descartar cambios"))) {
			saveFile();
		}
		return true;
	}
	return false;
}

void MainWindow::saveFile (const QString& ruta) {
	QFile file (ruta);
	if (file.open (QIODevice::WriteOnly | QIODevice::Text)) {
		file.write (ui->textEdit->toPlainText().toUtf8()); // devuelve Qstring y lo convierte a UTF8.
		statusBar()->showMessage (tr ("Archivo guardado exitósamente"), 3000);
	}
}

void MainWindow::fillList() {
	QStringList textToTemplateList;
	// << sobrecargado, hace función de append
	textToTemplateList << "<html>" << "</html>" <<  "<body>" << "</body>";
	ui->templateList->addItems (textToTemplateList);
}

void MainWindow::readSettings() {
	/// QSettings sirve para guardar configuraciones acerca del programa. Es multiplataforma. Funciona con el esquema llave/valor
	QSettings settings;
	resize(settings.value("MainWindow/Size", sizeHint()).toSize());
	restoreState(settings.value("MainWindow/Properties").toByteArray());
}

void MainWindow::writeSettings() {
	QSettings settings;
	settings.setValue("MainWindow/Size", QMainWindow::size());
	settings.setValue("MainWindow/Properties", QMainWindow::saveState());
}

void MainWindow::saveFile() {
	if (!rutaArchivo.isEmpty()) {
		saveFile (rutaArchivo);
	}
	else {
		saveFileAs();
	}
}

void MainWindow::saveFileAs() {
	// Guardar un archivo
	rutaArchivo = QFileDialog::getSaveFileName (this, tr ("Guardar Archivo como..."),
				  "./Tmp.txt", tr ("Texto (*. txt *.mplp)"));
	if (rutaArchivo.isEmpty()) {
		return;
	}
	saveFile (rutaArchivo);
}

void MainWindow::undo() {
	ui->textEdit->undo();
}

void MainWindow::redo() {
	ui->textEdit->redo();
}

void MainWindow::copy() {
	ui->textEdit->copy();
}

void MainWindow::about() {
	QMessageBox::about (this, tr ("Acerca de"), tr ("Pequeño editor de texto hecho en Qt\nSe asume que el texto tiene codificación UTF8\n\n""\t2014 Kevin Delgado"));
}

void MainWindow::updateStats() {
	QString text = ui->textEdit->toPlainText(); // Obtiene el texto de textEdit
	int chars = text.length();
	text = text.simplified(); // Trim
	int words = text.count (" ");
	if (!text.isEmpty()) {
		++words;
	}
	/// Forma para utilizar strings dinámicos
	QString output = tr ("Characters: %1, Words: %2").arg (chars).arg (words);
	statsLabel->setText (output);
}

void MainWindow::insertText (QListWidgetItem* item) {
	QString str = item->text();

	QTextCursor cursor = ui->textEdit->textCursor();
	cursor.insertText (str);
	ui->textEdit->setTextCursor (cursor);
}
