#include "mainwindow.h"
#include <QApplication>

int main (int argc, char* argv[]) {
	QApplication app (argc, argv);

	// Con esta información el QSettings sabe donde guardarlos
	QCoreApplication::setOrganizationName("Manchis");
	QCoreApplication::setApplicationName("Qt-SmallTextEditor");

	MainWindow mainWindow;
	mainWindow.show();

	return app.exec();
}
